create schema tp;

CREATE TABLE `tp`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));



CREATE TABLE `tp`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `quantity` VARCHAR(45) NULL,
  `price` DOUBLE NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `tp`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idClient` INT NULL,
  `product` VARCHAR(45) NULL,
  `quantity` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `tp`.`shipment` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `idClient` INT NULL,
    `name` VARCHAR(45) NULL,
    `address` VARCHAR(45) NULL,
    `product` VARCHAR(45) NULL,
    `quantity` INT NULL,
    `price` DOUBLE NULL,
    PRIMARY KEY (`id`));

