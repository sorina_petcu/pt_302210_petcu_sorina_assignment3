package tp;

import tp.presentation.Controller;

/**
 *
 * @author Sorina
 *
 */
public class Main {

    public static void main(String[] args) {
            new Controller().process();
    }
}
