package tp.bll;

import tp.dao.ShipmentDAO;
import tp.model.Shipment;

public class ShipmentBLL {

    ShipmentDAO shipmentDAO;

    public ShipmentBLL() throws ClassNotFoundException {
        this.shipmentDAO = new ShipmentDAO();
    }

    public void insert(Shipment shipment) {
        shipmentDAO.insert(shipment);
    }
}
