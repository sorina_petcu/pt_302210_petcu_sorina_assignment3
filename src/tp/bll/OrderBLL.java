package tp.bll;


import tp.dao.OrderDAO;
import tp.model.Order;

import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL {

    private OrderDAO orderDAO;

    public OrderBLL() throws ClassNotFoundException {
        orderDAO = new OrderDAO();
    }
     /**
      * returneaza lista de comenzi
      * @return
      */
    public List<Order> findAllOrders() {
        List<Order> orderList = orderDAO.findAllOrders();
        if (orderList == null) {
            throw new NoSuchElementException("No order was found in DB");
        }
        return orderList;
    }
    
    /**
     * plaseaza comanda
     * @param order
     * @return
     */

    public int placeOrder(Order order) {
        return orderDAO.place(order);
    }

}
