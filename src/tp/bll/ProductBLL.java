package tp.bll;

import tp.dao.ProductDAO;
import tp.model.Product;

import java.util.List;
import java.util.NoSuchElementException;


public class ProductBLL {
    private ProductDAO productDAO;

    public ProductBLL() throws ClassNotFoundException {
        productDAO = new ProductDAO();
    }
    
    /**
     * cauta produsul dupa nume si afiseaza un msj coresp daca nu exista
     * @param name
     * @return
     */

    public Product findByName(String name) {
        Product cs = productDAO.findByName(name);
        if (cs == null) {
            throw new NoSuchElementException("The product with name =" + name + " was not found!");
        }
        return cs;
    }
    
    /**
     * returneaza lista de produse
     * @return
     */

    public List<Product> findAllProducts() {
        List<Product> productList = productDAO.findAllProducts();
        if (productList == null) {
            throw new NoSuchElementException("No product was found in DB");
        }
        return productList;
    }

    public void insertProduct(Product product) {
        productDAO.insert(product);
    }

    public void deleteProduct(int id) {
        productDAO.delete(id);
    }

    public void updateProduct(Product product) {
        productDAO.update(product);
    }
}
