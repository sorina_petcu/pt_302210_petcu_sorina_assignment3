package tp.bll;

import tp.model.Client;
import tp.dao.ClientDAO;

import java.util.List;
import java.util.NoSuchElementException;

public class ClientBLL {
    private ClientDAO customerDAO;

    public ClientBLL() throws ClassNotFoundException {
        customerDAO = new ClientDAO();
    }
    
    /**
     * verifica daca exista clientul dupa nume
     * @param name
     * @return
     */
     

    public Client findClientByName(String name) {
        Client cs = customerDAO.findByName(name);
        if (cs == null) {
            throw new NoSuchElementException("The client with name =" + name + " was not found!");
        }
        return cs;
    }
    
    /**
     * returneaza lista de clienti
     * @return
     */

    public List<Client> findAllClients() {
        List<Client> clientList = customerDAO.findAll();
        if (clientList == null) {
            throw new NoSuchElementException("No client was found in DB!");
        }
        return clientList;
    }
    
    /**
     * inserare si sterge client
     * @param customer
     * @return
     */

    public int insertClient(Client customer) {
        return customerDAO.insert(customer);
    }

    public int deleteClient(int id) {
        return customerDAO.delete(id);
    }


}
