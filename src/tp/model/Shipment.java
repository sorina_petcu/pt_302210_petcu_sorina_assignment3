package tp.model;

public class Shipment {
    private int id;
    private int idClient;
    private String name;
    private String address;
    private String product;
    private int quantity;
    private double price;
    
    /**
     * shiptemnt contine toate atributele celorlalte clase si am implementat getteri si stteri pt fiecare
     * @param idClient
     * @param name
     * @param address
     * @param product
     * @param quantity
     * @param price
     */


    public Shipment(int idClient, String name, String address, String product, int quantity, double price) {
        this.idClient = idClient;
        this.name = name;
        this.address = address;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
