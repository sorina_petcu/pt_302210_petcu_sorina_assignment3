package tp.model;


public class Client {
    private int id;
    private String name;
    private String address;


    public Client(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Client() {
    }

    public Client(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }


    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }


}
