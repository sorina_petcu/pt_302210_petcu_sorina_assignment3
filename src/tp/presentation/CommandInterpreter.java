package tp.presentation;

import tp.bll.ClientBLL;
import tp.bll.OrderBLL;
import tp.bll.ProductBLL;
import tp.bll.ShipmentBLL;
import tp.model.Client;
import tp.model.Order;
import tp.model.Product;
import tp.model.Shipment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class CommandInterpreter {

    ClientBLL clientBLL;
    ProductBLL productBLL;
    OrderBLL orderBLL;
    ShipmentBLL shipmentBLL;
    PdfReport pdf = new PdfReport();

    public CommandInterpreter(ClientBLL clientBLL, ProductBLL productBLL, OrderBLL orderBLL,ShipmentBLL shipmentBLL) {
        this.clientBLL = clientBLL;
        this.productBLL = productBLL;
        this.orderBLL = orderBLL;
        this.shipmentBLL = shipmentBLL;
    }
    
    /**
     * citire din fisierul txt
     * @param fileName
     */

    void process(String fileName) {
        File file = new File(fileName);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner.hasNextLine()) {
            String data = scanner.nextLine();
            executeCommand(data);

        }
    }
    /**
     * metoda care pentru fiecare caz genereaza rezultate fiecarei actiuni: insert, delete, report, order si face split
     * @param data
     */

    private void executeCommand(String data) {
        String[] action = data.split(":");
        String[] command = action[0].split(" ");

        switch (command[0]) {
            case "Insert": {
                String tableName = command[1].toLowerCase();
                String[] parameters = action[1].split(",");
                if (tableName.equals("client")) {
                    String name = parameters[0];
                    String address = parameters[1];
                    Client client = new Client(name, address);
                    clientBLL.insertClient(client);
                } else if (tableName.equals("product")) {
                    String productName = parameters[0];
                    int quantity = Integer.parseInt(parameters[1].trim());
                    double price = Double.parseDouble(parameters[2].trim());
                    Product product = new Product(productName, price, quantity);
                    productBLL.insertProduct(product);
                }
                break;
            }
            case "Delete": {
                String tableName = command[1].toLowerCase();
                String[] parameters = action[1].split(",");
                if (tableName.equals("client")) {
                    String name = parameters[0];
                    String address = parameters[1];
                    Client client = clientBLL.findClientByName(name);
                    clientBLL.deleteClient(client.getId());
                } else if (tableName.equals("product")) {
                    String productName = parameters[0];
                    Product product = productBLL.findByName(productName);
                    productBLL.deleteProduct(product.getId());
                }
                break;
            }
            case "Report":
                String tableName = command[1].toLowerCase();
                String timestamp = String.valueOf(System.currentTimeMillis());
                switch (tableName) {
                    case "client":
                        List<Client> clients = clientBLL.findAllClients();
                        pdf.reportClients("Clients-"+timestamp+".pdf", clients);
                        break;
                    case "product":
                        List<Product> products = productBLL.findAllProducts();
                        pdf.reportProducts("Products-"+timestamp+".pdf", products);
                        break;
                    case "order":
                        List<Order> orders = orderBLL.findAllOrders();
                        pdf.reportOrders("Orders-"+timestamp+".pdf", orders);
                        break;
                }
                break;
            case "Order": {
                String[] parameters = action[1].split(",");
                String name = parameters[0];
                String productName = parameters[1];
                int quantity = Integer.parseInt(parameters[2].trim());
                Product product = productBLL.findByName(productName);
                int actualQuantity = product.getQuantity() - quantity;
                if (actualQuantity < 0) {
                    pdf.writeMessage("OutOfStock-" + productName + ".pdf");
                    return;
                }
                Client client = clientBLL.findClientByName(name);
                product.setQuantity(actualQuantity);
                productBLL.updateProduct(product);
                Order order = new Order(client.getId(), productName, quantity);
                orderBLL.placeOrder(order);
                Shipment shipment= new Shipment(client.getId(),client.getName(),client.getAddress(),productName,actualQuantity,product.getPrice());
                shipmentBLL.insert(shipment);
                break;
            }
        }

    }

}
