package tp.presentation;

import java.io.FileOutputStream;
import java.util.List;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import tp.model.Client;
import tp.model.Order;
import tp.model.Product;

/**
 * toate metodele sunt pentru crearea si editarea continului pdf-urilor si continutului lor
 * @author Sorina
 *
 */
class PdfReport {
    private String FILE;
    private final static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private final static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);

    public void reportClients(String path, List<Client> content) {
        FILE = path;
        try {
            Document document = new Document();
            addMetaData(document);
            addContentToClientReport(document, content);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reportProducts(String path, List<Product> content) {
        FILE = path;
        try {
            Document document = new Document();
            addMetaData(document);
            addContentToProductReport(document, content);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeMessage(String path) {
        FILE = path;
        try {
            Document document = new Document();
            addMetaData(document);
            Anchor anchor = new Anchor("Out of Stock", catFont);
            Chapter chapter = new Chapter(new Paragraph(anchor),1);
            document.add(chapter);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reportOrders(String path, List<Order> content) {
        FILE = path;
        try {
            Document document = new Document();
            addMetaData(document);
            addContentToOrderReport(document, content);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMetaData(Document document) throws Exception {
        PdfWriter.getInstance(document, new FileOutputStream(FILE));
        document.open();
        document.addTitle("PDF");
        document.addSubject("Model");
        document.addKeywords("Java, PDF, Model");
        document.addAuthor("Sorina Petcu");
        document.addCreator("Sorina Petcu");
    }

    private void addContentToClientReport(Document document, List<Client> clients) throws DocumentException {
        Anchor anchor = new Anchor("Database", catFont);
        anchor.setName("First Chapter");
        Chapter chapter = new Chapter(new Paragraph(anchor), 1);  // al doilea numar e nr chapterului
        Paragraph subPara = new Paragraph("Clients", subFont);
        Section subCatPart = chapter.addSection(subPara);
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph);
        subCatPart.add(paragraph);
        createClientsTable(subCatPart, clients); 
        document.add(chapter);  
    }

    private void addContentToProductReport(Document document, List<Product> products) throws DocumentException {
        Anchor anchor = new Anchor("Database", catFont);
        anchor.setName("First Chapter");
        Chapter chapter = new Chapter(new Paragraph(anchor), 1);  
        Paragraph subPara = new Paragraph("Products", subFont);
        Section subCatPart = chapter.addSection(subPara);
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph);
        subCatPart.add(paragraph);
        createProductsTable(subCatPart, products); 
        document.add(chapter);  
    }

    private void addContentToOrderReport(Document document, List<Order> orders) throws DocumentException {
        Anchor anchor = new Anchor("Database", catFont);
        anchor.setName("First Chapter");
        Chapter chapter = new Chapter(new Paragraph(anchor), 1);  
        Paragraph subPara = new Paragraph("Orders", subFont);
        Section subCatPart = chapter.addSection(subPara);
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph);
        subCatPart.add(paragraph);
        createOrdersTable(subCatPart, orders); 
        document.add(chapter);  
    }

    private void createProductsTable(Section subCatPart, List<Product> products) {
        PdfPTable table = new PdfPTable(4);
        addCell(table, "Id");
        addCell(table, "Name");
        addCell(table, "Quantity");
        addCell(table, "Price");
        table.setHeaderRows(1);
        for (Product p : products) {
            table.addCell(p.getId() + "");
            table.addCell(p.getName());
            table.addCell(p.getQuantity() + "");
            table.addCell(p.getPrice() + "");
        }
        subCatPart.add(table);
    }

    private void createClientsTable(Section subCatPart, List<Client> clients) {
        PdfPTable table = new PdfPTable(3);
        addCell(table, "Id");
        addCell(table, "Name");
        addCell(table, "Address");
        table.setHeaderRows(1);
        for (Client c : clients) {
            table.addCell(c.getId() + "");
            table.addCell(c.getName());
            table.addCell(c.getAddress());
        }
        subCatPart.add(table);
    }
    
    /**
     * creez celulele pdf-urilor
     * @param subCatPart
     * @param orders
     */

    private void createOrdersTable(Section subCatPart, List<Order> orders) {
        PdfPTable table = new PdfPTable(4);
        addCell(table, "Id");
        addCell(table, "Client Id");
        addCell(table, "ProductName");
        addCell(table, "Quantity");
        table.setHeaderRows(1);
        for (Order o : orders) {
            table.addCell(o.getId() + "");
            table.addCell(o.getIdClient() + "");
            table.addCell(o.getProduct());
            table.addCell(o.getQuantity() + "");
        }
        subCatPart.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph) {
        for (int i = 0; i < 5; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private void addCell(PdfPTable table, String content) {
        PdfPCell cell = new PdfPCell(new Phrase(content));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);
    }
}