package tp.presentation;

import tp.bll.ClientBLL;
import tp.bll.OrderBLL;
import tp.bll.ProductBLL;
import tp.bll.ShipmentBLL;

public class Controller {

    private ClientBLL clientBLL = null;
    private ProductBLL productBLL = null;
    private OrderBLL orderBLL = null;
    private ShipmentBLL shipmentBLL = null;

    public Controller() {
        try {
            clientBLL = new ClientBLL();
            productBLL = new ProductBLL();
            orderBLL = new OrderBLL();
            shipmentBLL = new ShipmentBLL();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void process() {
        CommandInterpreter ci = new CommandInterpreter(clientBLL, productBLL, orderBLL,shipmentBLL);
        ci.process("input.txt");

    }
}
