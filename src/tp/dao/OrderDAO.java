package tp.dao;

import tp.model.Order;

import java.util.List;

/**
 * Clasa extinde AbstractDao contine statementuri de modificare si interogare a tabelelei Order
 * Contine insert, delete update si select
 */
public class OrderDAO extends AbstractDAO<Order> {
    private static final String insertStatement = "INSERT INTO `order` (id,idClient,product,quantity)"
            + " VALUES (?,?,?,?)";
    private final static String findStatement = "SELECT * FROM `order` where id_order = ?";
    private final static String findAllOrdersStatement = "SELECT * FROM `order`";
    private final static String deleteString = "DELETE FROM `order` where id_order = ?";
    private final static String updateString = "UPDATE `order` set id_client=?,id=?,quantity=? where id_order = ?";

    public OrderDAO() throws ClassNotFoundException {

    }


    public int place(Order order) {
        return super.insert(order, insertStatement);
    }


    public List<Order> findAllOrders() {
        return super.findAll(findAllOrdersStatement);
    }
}
