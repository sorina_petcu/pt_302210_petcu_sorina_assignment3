package tp.dao;

import tp.model.Shipment;

/**
 * shipment functioneaza ca un master table
 * @author Sorina
 *
 */

public class ShipmentDAO extends AbstractDAO<Shipment> {

    private static final String insertStatement = "INSERT INTO shipment (id,idClient,name,address,product,quantity,price)"
            + " VALUES (?,?,?,?,?,?,?)";


    public ShipmentDAO() throws ClassNotFoundException {
    }

    public int insert(Shipment shipment) {
        return super.insert(shipment, insertStatement);
    }
}
