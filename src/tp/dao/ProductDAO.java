package tp.dao;


import tp.model.Product;

import java.util.List;

/**
 * Clasa extinde AbstractDao contine statementuri de modificare si interogare a tabelelei Product
 * Contine insert, delete update si select
 */
public class ProductDAO extends AbstractDAO<Product> {

    private static final String insertStatement = "INSERT INTO product (id,name,quantity,price)"
            + " VALUES (?,?,?,?)";
    private final static String findStatement = "SELECT * FROM product where id = ?";
    private final static String findAllProductsStatement = "SELECT * FROM product";
    private final static String findByNameStatement = "SELECT * FROM product where name = ?";
    private final static String deleteString = "DELETE FROM product where id = ?";
    private final static String updateString = "UPDATE product set name=?,quantity=?,price=? where id = ?";


    public ProductDAO() throws ClassNotFoundException {
    }


    public int insert(Product product) {
        return super.insert(product, insertStatement);
    }

    public List<Product> findAllProducts() {
        return super.findAll(findAllProductsStatement);
    }

    public Product findByName(String name) {
        return super.findByName(findByNameStatement, name);
    }

    public int delete(int id) {
        return super.delete(deleteString, id);
    }

    public int update(Product product) {
        return super.update(product, updateString);
    }

}
