package tp.dao;


import tp.connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractDAO<T> {

    Class<T> specificGenericClass;
    private final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    @SuppressWarnings("unckecked")
    public AbstractDAO() throws ClassNotFoundException {
        this.specificGenericClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    public int insert(T t, String insertStatementString) {
        Connection dbConnection = ConnectionFactory.getConnection();
        int no = 1;
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        java.beans.PropertyDescriptor objPropertyDescriptor;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            Field[] fields = specificGenericClass.getDeclaredFields();
            for (Field f : fields) {
                try {
                    objPropertyDescriptor = new java.beans.PropertyDescriptor(f.getName(), specificGenericClass);
                    Method method = objPropertyDescriptor.getReadMethod();
                    if (!method.getReturnType().isPrimitive()) {
                        insertStatement.setString(no, (String) method.invoke(t, null));
                    } else {
                        String y = method.invoke(t, null).toString();
                        if (y.contains(".")) {
                            insertStatement.setDouble(no, Double.parseDouble(y));
                        } else {
                            insertStatement.setInt(no, Integer.parseInt(y));
                        }
                    }
                    no++;
                } catch (IntrospectionException e) {
                    e.printStackTrace();
                }
            }
            insertStatement.executeUpdate();
            insertedId = 1;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Wrong: " + e.getMessage());
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public T findById(String toFind, int id) {
        T actual = null;
        try {
            actual = specificGenericClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet resultSet = null;
        Field[] fields;
        try {
            findStatement = dbConnection.prepareStatement(toFind);
            fields = specificGenericClass.getDeclaredFields();
            findStatement.setInt(1, id);
            resultSet = findStatement.executeQuery();
            resultSet.next();
            processFields(actual, resultSet, fields);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "AbstractDAO:findId " + e.getMessage());
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }

        return actual;

    }

    public T findByName(String toFind, String name) {
        T actual = null;
        try {
            actual = specificGenericClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet resultSet = null;
        Field[] fields;
        try {
            findStatement = dbConnection.prepareStatement(toFind);
            fields = specificGenericClass.getDeclaredFields();
            findStatement.setString(1, name);
            resultSet = findStatement.executeQuery();
            resultSet.next();
            processFields(actual, resultSet, fields);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "AbstractDAO:findByName " + e.getMessage());
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return actual;
    }

    public int delete(String deleteStatement, int id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement delStatement = null;
        int resultSet = 0;
        try {
            delStatement = dbConnection.prepareStatement(deleteStatement);
            delStatement.setInt(1, id);
            resultSet = delStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Wrong " + e.getMessage());
        } finally {
            ConnectionFactory.close(delStatement);
            ConnectionFactory.close(dbConnection);
        }
        return resultSet;
    }

    public int update(T t, String updateString) {
        Connection dbConnection = ConnectionFactory.getConnection();
        int done = -1;
        PreparedStatement updateStatement = null;
        java.beans.PropertyDescriptor objPropertyDescriptor;
        try {
            updateStatement = dbConnection.prepareStatement(updateString, Statement.RETURN_GENERATED_KEYS);
            Field[] fields = specificGenericClass.getDeclaredFields();

            int no = fields.length;
            for (Field f : fields) {
                try {
                    objPropertyDescriptor = new java.beans.PropertyDescriptor(f.getName(), specificGenericClass);
                    Method method = objPropertyDescriptor.getReadMethod();
                    if (!method.getReturnType().isPrimitive()) {
                        updateStatement.setString(no, (String) method.invoke(t, null));
                    } else {
                        String y = method.invoke(t, null).toString();
                        if (y.contains(".")) {
                            updateStatement.setDouble(no, Double.parseDouble(y));
                        } else {
                            updateStatement.setInt(no, Integer.parseInt(y));
                        }
                    }
                    no++;
                    if (no == fields.length + 1) {
                        no = 1;
                    }
                } catch (IntrospectionException e) {
                    e.printStackTrace();
                }
            }
            updateStatement.executeUpdate();
            done = 1;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Wrong: " + e.getMessage());
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return done;
    }

    protected List<T> findAll(String findAllStatement) {
        List<T> dbEntities = new ArrayList<>();
        T actual = null;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet resultSet = null;
        Field[] fields;

        try {
            findStatement = dbConnection.prepareStatement(findAllStatement);
            fields = specificGenericClass.getDeclaredFields();
            resultSet = findStatement.executeQuery();
            while (resultSet.next()) {
                try {
                    actual = specificGenericClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                processFields(actual, resultSet, fields);
                dbEntities.add(actual);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "AbstractDAO:findAll " + e.getMessage());
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return dbEntities;
    }

    private void processFields(T actual, ResultSet resultSet, Field[] fields) throws IntrospectionException, SQLException, IllegalAccessException, InvocationTargetException {
        java.beans.PropertyDescriptor objPropertyDescriptor;
        Method method;
        Object toAdd;
        for (Field f : fields) {
            objPropertyDescriptor = new java.beans.PropertyDescriptor(f.getName(), specificGenericClass);
            method = objPropertyDescriptor.getWriteMethod();
            toAdd = resultSet.getObject(f.getName());

            if (f.getType().isPrimitive()) {
                if (toAdd instanceof String) {
                    if (((String) toAdd).contains(".")) {
                        toAdd = Double.parseDouble("" + toAdd);
                    } else {
                        toAdd = Integer.parseInt("" + toAdd);
                    }
                }
                method.invoke(actual, toAdd);
            } else {
                method.invoke(actual, (String) toAdd);
            }
        }
    }
}
