package tp.dao;


import tp.model.Client;

import java.util.List;

/**
 * Clasa extinde AbstractDao contine statementuri de modificare si interogare a tabelelei Client
 * Contine insert, delete update si select
 */
public class ClientDAO extends AbstractDAO<Client> {



    private static final String insertStatementString = "INSERT INTO client (id,name,address)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM client where id = ?";
    private final static String findAllClients = "SELECT * FROM client";
    private final static String findByNameStatementString = "SELECT * FROM client where name = ?";
    private final static String deleteString = "DELETE FROM client where id = ?";
    private final static String updateString = "UPDATE client set name=?,address=?,phone=?,email=? where id = ?";

    public ClientDAO() throws ClassNotFoundException {
    }

    public  int insert(Client client){
       return super.insert(client,insertStatementString);
    }


    public Client findByName(String name){
        return super.findByName(findByNameStatementString,name);
    }

    public List<Client> findAll(){
        return super.findAll(findAllClients);
    }
    public int delete(int id) { return super.delete(deleteString,id);}

}
